angular.module('songhop.controllers', ['ionic', 'songhop.services'])


/*
Controller for the discover page
*/

.controller('SplashCtrl', function($scope, $state, User) {

  // attempt to signup/login via User.auth
  $scope.submitForm = function(username, signingUp) {
    User.auth(username, signingUp).then(function(){
      // session is now set, so lets redirect to discover page
      $state.go('tab.discover');

    }, function() {
      // error handling here
      alert('Hmm... try another username.');

    });
  }

})



.controller('DiscoverCtrl', function($scope,$ionicLoading, $timeout, User,Recommendations) {

  // helper functions for loading
  var showLoading = function() {
    $ionicLoading.show({
      template: '<i class="ion-loading-c"></i>',
      noBackdrop: true
    });
  }

  var hideLoading = function() {
    $ionicLoading.hide();
  }

  // set loading to true first time while we retrieve songs from server.
  showLoading();



  Recommendations.init()
    .then(function(){
      hideLoading();
      $scope.currentSong = Recommendations.queue[0];
      Recommendations.playCurrentSong();
    })
    .then(function(){
      // turn loading off
      hideLoading();
      $scope.currentSong.loaded = true;
    });

    // fired when we favorite / skip a song.
    $scope.sendFeedback = function (bool) {

      // first, add to favorites if they favorited
      if (bool) User.addSongToFavorites($scope.currentSong);

      // set variable for the correct animation sequence
      $scope.currentSong.rated = bool;
      $scope.currentSong.hide = true;

      // prepare the next song
      Recommendations.nextSong();

      // update current song in scope, timeout to allow animation to complete
      $timeout(function() {
        $scope.currentSong = Recommendations.queue[0];
        $scope.currentSong.loaded = false;
      }, 250);

      Recommendations.playCurrentSong().then(function() {
        $scope.currentSong.loaded = true;
      });

    }

})


/*
Controller for the favorites page
*/
.controller('FavoritesCtrl', function($scope, $window, User) {

  $scope.username = User.username;

  // get the list of our favorites from the user service
  $scope.favorites = User.favorites;
  $scope.removeSong = function(song, index) {
   User.removeSongFromFavorites(song, index);
 }
 $scope.openSong = function(song) {
  $window.open(song.open_url, "_system");
}


})



/*
Controller for our tab bar
*/
.controller('TabsCtrl', function($scope, User,$window, Recommendations) {

  $scope.logout = function() {
  User.destroySession();

  // instead of using $state.go, we're going to redirect.
  // reason: we need to ensure views aren't cached.
  $window.location.href = 'index.html';
}


  $scope.favCount = User.favoriteCount;

  // method to reset new favorites to 0 when we click the fav tab
  $scope.enteringFavorites = function() {
    User.newFavorites = 0;
    Recommendations.haltAudio();
  }

  $scope.leavingFavorites = function() {
  Recommendations.init();
}


});
